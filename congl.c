#include "congl.h"

#include <stdio.h>

void conglMoveCursorUp (int ammount)
{
	printf("[%dA",ammount);
	fflush(stdout);
}

void conglMoveCursorDown (int ammount)
{
	printf("[%dB",ammount);
	fflush(stdout);
}

void conglMoveCursorRight (int ammount)
{
	printf("[%dC",ammount);
	fflush(stdout);
}

void conglMoveCursorLeft (int ammount)
{
	printf("[%dD",ammount);
	fflush(stdout);
}

void conglMoveCursor(int line, int column)
{
	printf("[%d;%dH",line, column);
	fflush(stdout);
}

void conglClearScreen(void)
{
	printf("[2J");
	fflush(stdout);
}

void conglSetBg(conglColor_t color)
{
	printf("[4%dm", color);
	fflush(stdout);
}

void conglSetFg(conglColor_t color)
{
	printf("[3%dm", color);
	fflush(stdout);
}

void conglReset(void)
{
	printf("[m");
	fflush(stdout);
}

void conglDrawChar(char ch)
{
	printf("%c",ch);
	fflush(stdout);
}

void conglDrawCharFull(int line, int column, char ch, conglColor_t fg, conglColor_t bg)
{
	conglSetFg(fg);
	conglSetBg(bg);
	conglMoveCursor(line, column);
	conglDrawChar(ch);
	conglMoveCursor(line, column);
}
