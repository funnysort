#ifndef CONGL_H
#define CONGL_H

typedef enum {congl_black = 0, congl_red, congl_green, congl_yellow, congl_blue, congl_magenta, congl_cyan, congl_white} conglColor_t;

void conglMoveCursorUp(int ammount);
void conglMoveCursorDown(int ammount);
void conglMoveCursorRight(int ammount);
void conglMoveCursorLeft(int ammount);
void conglMoveCursor(int line, int column);
void conglClearScreen(void);
void conglSetBg(conglColor_t color);
void conglSetFg(conglColor_t color);
void conglReset(void);
void conglDrawChar(char ch);
void conglDrawCharFull(int line, int column, char ch, conglColor_t fg, conglColor_t bg);

#endif /* CONGL_H */
